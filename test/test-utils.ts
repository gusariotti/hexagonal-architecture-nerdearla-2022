import request from "supertest";
import type { FastifyInstanceWithTypeBox } from "../src/shared/types";
import { Clock } from "../src/shared/clock";
import got from "got/dist/source";
import type {
  DepositRequestType,
  WithdrawRequestType,
  BuyRequestType,
} from "../src/accounts/accountTypes";
import { ARS } from "../src/shared/money";

// NOTE clases y funciones de utilidad para las pruebas
export class TestClock extends Clock {
  nowDate: Date;
  constructor(nowDate: Date) {
    super();
    this.nowDate = nowDate;
  }

  override now() {
    return this.nowDate;
  }

  advance(miliseconds: number) {
    this.nowDate.setTime(this.nowDate.getTime() + miliseconds);
    return this;
  }
}

export async function getAccount(
  app: FastifyInstanceWithTypeBox,
  createAccountResponse: any
) {
  return await request(app.server)
    .get(`/account/${createAccountResponse.body.accountId}`)
    .set("Accept", "application/json");
}

export async function createAccount(
  app: FastifyInstanceWithTypeBox,
  user: any
) {
  return await request(app.server)
    .post("/account")
    .send(user)
    .set("Accept", "application/json");
}

export async function makeADeposit(
  app: FastifyInstanceWithTypeBox,
  depositRequest: DepositRequestType
) {
  return await request(app.server)
    .post("/account/deposit")
    .send(depositRequest)
    .set("Accept", "application/json");
}

export async function makeWithdraw(
  app: FastifyInstanceWithTypeBox,
  withdrawRequest: WithdrawRequestType
) {
  return await request(app.server)
    .post("/account/withdraw")
    .send(withdrawRequest)
    .set("Accept", "application/json");
}

export async function makeBuyOrder(
  app: FastifyInstanceWithTypeBox,
  buyOrder: BuyRequestType
) {
  return await request(app.server)
    .post("/account/buy")
    .send(buyOrder)
    .set("Accept", "application/json");
}

export function withdrawRequestBody(amount: number, account: any) {
  const withdraw = {
    currency: "ARS",
    amount: amount,
  };

  const withdrawRequest = {
    accountId: account.body.accountId,
    withdraw: withdraw,
  };
  return withdrawRequest;
}

export function depositRequestBody(amount: number, account: any) {
  const deposit = new ARS(amount);

  const depositRequest = {
    accountId: account.body.accountId,
    deposit: deposit,
  };
  return depositRequest;
}

export function buyOrderBody(
  account: any,
  galiXbueSymbol: string,
  amount: number
) {
  return {
    accountId: account.body.accountId,
    symbol: galiXbueSymbol,
    investment: new ARS(amount),
  };
}

export function marketSymbolValueFakeResponse(
  highValue: number,
  lowValue: number,
  symbol: string
) {
  return {
    pagination: {
      limit: 100,
      offset: 0,
      count: 1,
      total: 1,
    },
    data: [
      {
        open: 23,
        high: highValue,
        low: lowValue,
        close: 23,
        volume: null,
        adj_high: null,
        adj_low: null,
        adj_close: 23,
        adj_open: null,
        adj_volume: null,
        split_factor: 1,
        dividend: 0,
        symbol: symbol,
        exchange: "XBUE",
        date: "2022-10-07T00:00:00+0000",
      },
    ],
  };
}

export function createGotHttpError(
  errorMessage: string,
  apiResponse: { body: { error: { code: string; message: string } } }
) {
  const fakeResponse = Error(errorMessage);
  fakeResponse.name = "HTTPError";
  (fakeResponse as any).code = "ERR_NON_2XX_3XX_RESPONSE";

  (fakeResponse as any).response = apiResponse;
  return fakeResponse;
}

export function gotMockedReturnValue(response: any) {
  return {
    json: () => Promise.resolve(response),
  } as any;
}

export function gotMockGetResponseWith(response: any) {
  const mockedGot = jest.mocked(got);
  // use case #1 - using got module directly
  mockedGot.mockReturnValue(gotMockedReturnValue(response));
}

export function anObjectContainingCashBalanceEqual(amount: ARS): any {
  return expect.objectContaining({ cashBalance: amount });
}

export const aDepositResponseWithATransactionId = {
  transactionId: expect.toBeString(),
};

export function delay(time: number) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

export function anObjectContainingBuyOrderResult(
  galiXbueSymbol: string,
  buyedStock: number,
  arsAmountCloseTo: number,
  arsAmountDigits: number
): any {
  return expect.objectContaining({
    transactionId: expect.toBeString(),
    symbol: galiXbueSymbol,
    buyedStock: buyedStock,
    realInvestment: arsCloseTo(arsAmountCloseTo, arsAmountDigits),
  });
}

export function arsCloseTo(amount: number, digits: number) {
  return {
    currency: "ARS",
    amount: expect.closeTo(amount, digits),
  };
}
