import { expect } from "@jest/globals";
import type { MatcherFunction, MatcherContext } from "expect";
import type { Response } from "supertest";

class PartialErrorRequest {
  statusCode: number;
  error: string;
  message: string;

  static FromResponse(actual: Response) {
    const { error, message } = actual.body as any;
    return new PartialErrorRequest(actual.statusCode, error, message);
  }

  static BadRequestWithMessage(message: string) {
    return new PartialErrorRequest(400, "Bad Request", message);
  }

  constructor(statusCode: number, error: string, message: string) {
    this.statusCode = statusCode;
    this.error = error;
    this.message = message;
  }

  equals(other: unknown) {
    return (
      other instanceof PartialErrorRequest &&
      this.statusCode === other.statusCode &&
      this.error === other.error &&
      this.message === other.message
    );
  }
}

class BadRequestMatcherResult {
  possibleBadRequest: PartialErrorRequest;
  expectedBadRequest: PartialErrorRequest;

  static fromParams(actual: unknown, message: unknown) {
    // TypeGuard no puede ser negadas
    if (BadRequestMatcherResult.isNotAResponse(actual))
      throw new Error("Value is not a response!");
    if (typeof message !== "string")
      throw new Error("Error! message parameter should be a string");
    const actualAsBadRequest = PartialErrorRequest.FromResponse(actual);
    const expectedPartialBadRequest =
      PartialErrorRequest.BadRequestWithMessage(message);
    return new BadRequestMatcherResult(
      actualAsBadRequest,
      expectedPartialBadRequest
    );
  }

  constructor(
    possibleBadRequest: PartialErrorRequest,
    expectedBadRequest: PartialErrorRequest
  ) {
    this.possibleBadRequest = possibleBadRequest;
    this.expectedBadRequest = expectedBadRequest;
  }

  static isAResponse(possibleResponse: unknown): possibleResponse is Response {
    const isAnObject =
      possibleResponse != null &&
      typeof possibleResponse === "object" &&
      !Array.isArray(possibleResponse);
    const { statusCode, body } = possibleResponse as any;
    return isAnObject && statusCode != null && body != null;
  }

  static isNotAResponse(
    possibleResponse: unknown
  ): possibleResponse is Exclude<any, Response> {
    return !this.isAResponse(possibleResponse);
  }

  isAPass() {
    return this.expectedBadRequest.equals(this.possibleBadRequest);
  }
  jestMessageFunction(jestContext: MatcherContext) {
    return () =>
      `expected response not to be a Bad Request.\n${jestContext.utils.diff(
        this.expectedBadRequest,
        this.possibleBadRequest
      )}`;
  }
}

const toBeABadRequestWithMessage: MatcherFunction<[message: unknown]> =
  function (actual, message) {
    const result = BadRequestMatcherResult.fromParams(actual, message);
    return {
      message: result.jestMessageFunction(this),
      pass: result.isAPass(),
    };
  };

expect.extend({
  toBeABadRequestWithMessage,
});
declare module "expect" {
  interface AsymmetricMatchers {
    toBeABadRequestWithMessage(message: string): void;
  }
  interface Matchers<R> {
    toBeABadRequestWithMessage(message: string): R;
  }
}

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace jest {
    interface Matchers<R> {
      toBeABadRequestWithMessage(message: string): R;
    }
    interface AsymmetricMatchers {
      toBeABadRequestWithMessage(message: string): void;
    }
  }
}
