import { expect } from "@jest/globals";
import type { MatcherFunction, MatcherContext } from "expect";
import type { Response } from "supertest";

type ValiJsonTypes = null | number | string | boolean | object;

class PartialOkRequest {
  statusCode: number;
  body: ValiJsonTypes;

  static FromResponse(actual: Response) {
    return new PartialOkRequest(actual.statusCode, actual.body);
  }

  static OkRequestWithBody(body: ValiJsonTypes) {
    return new PartialOkRequest(200, body);
  }

  constructor(statusCode: number, body: ValiJsonTypes) {
    this.statusCode = statusCode;
    this.body = body;
  }
}
function isValidJsonType(
  possibleJsonType: unknown
): possibleJsonType is ValiJsonTypes {
  return (
    possibleJsonType === null ||
    typeof possibleJsonType === "boolean" ||
    typeof possibleJsonType === "number" ||
    typeof possibleJsonType === "string" ||
    typeof possibleJsonType === "object"
  );
}

class OkRequestWithBodyMatcherResult {
  possibleOkRequest: PartialOkRequest;
  expectedOkRequest: PartialOkRequest;

  static fromParams(actual: unknown, body: unknown) {
    // TypeGuard no puede ser negadas
    if (OkRequestWithBodyMatcherResult.isNotAResponse(actual))
      throw new Error("Value is not a response!");
    if (!isValidJsonType(body))
      throw new Error("Error! body parameter should be defined");
    const actualAsOkRequest = PartialOkRequest.FromResponse(actual);
    const expectedPartialOkRequest = PartialOkRequest.OkRequestWithBody(body);
    return new OkRequestWithBodyMatcherResult(
      actualAsOkRequest,
      expectedPartialOkRequest
    );
  }

  constructor(
    possibleBadRequest: PartialOkRequest,
    expectedBadRequest: PartialOkRequest
  ) {
    this.possibleOkRequest = possibleBadRequest;
    this.expectedOkRequest = expectedBadRequest;
  }

  static isAResponse(possibleResponse: unknown): possibleResponse is Response {
    const isAnObject =
      possibleResponse != null &&
      typeof possibleResponse === "object" &&
      !Array.isArray(possibleResponse);
    const { statusCode, body } = possibleResponse as any;
    return isAnObject && statusCode != null && body != null;
  }

  static isNotAResponse(
    possibleResponse: unknown
  ): possibleResponse is Exclude<any, Response> {
    return !this.isAResponse(possibleResponse);
  }

  isAPass(jestContext: MatcherContext) {
    return jestContext.equals(this.possibleOkRequest, this.expectedOkRequest);
  }
  jestMessageFunction(jestContext: MatcherContext) {
    return () =>
      `expected response not to be a Bad Request.\n${jestContext.utils.diff(
        this.expectedOkRequest,
        this.possibleOkRequest
      )}`;
  }
}

const toBeAnOkResponseWithBody: MatcherFunction<[body: unknown]> = function (
  actual,
  body
) {
  const result = OkRequestWithBodyMatcherResult.fromParams(actual, body);
  this.equals;
  return {
    message: result.jestMessageFunction(this),
    pass: result.isAPass(this),
  };
};

expect.extend({
  toBeAnOkResponseWithBody,
});
declare module "expect" {
  interface AsymmetricMatchers {
    toBeAnOkResponseWithBody(body: any): void;
  }
  interface Matchers<R> {
    toBeAnOkResponseWithBody(body: any): R;
  }
}

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace jest {
    interface Matchers<R> {
      toBeAnOkResponseWithBody(body: any): R;
    }
    interface AsymmetricMatchers {
      toBeAnOkResponseWithBody(body: any): void;
    }
  }
}
