import { Account } from "../src/accounts/account";
import { AccountSystemError } from "../src/accounts/accountErrors";
import { AccountSystem } from "../src/accounts/accountSystem";
import {
  Broker,
  BuyOrder,
  FinancialInstrumentValue,
} from "../src/accounts/broker";
import { Ledger, Transaction } from "../src/accounts/ledger";
import type { Clock } from "../src/shared/clock";
import { ARS, Money, USD } from "../src/shared/money";
import { TestClock } from "./test-utils";

class TestLedger implements Ledger {
  clock: Clock;
  userIds: Set<string>;
  accounts: Map<number, Account>;

  constructor(clock: Clock) {
    this.clock = clock;
    this.userIds = new Set<string>();
    this.accounts = new Map<number, Account>();
  }
  findAccountWith(id: number): Promise<Account | null> {
    return Promise.resolve(this.accounts.get(id) ?? null);
  }
  createAccountFor(userId: string): Promise<Account> {
    if (this.userIds.has(userId)) {
      throw new AccountSystemError("User already have an account.");
    } else {
      this.userIds.add(userId);
      const newAccount = new Account(
        this.accounts.size + 1,
        userId,
        new Date(this.clock.now()),
        ARS.Zero(),
        ARS.Zero()
      );
      this.accounts.set(newAccount.id, newAccount);
      return Promise.resolve(newAccount);
    }
  }
  registerCashBalance(_account: Account): Promise<Transaction> {
    return Promise.resolve(Transaction.createWithUUIDv4Id());
  }
}
class TestBroker implements Broker {
  private svbCalls = 0;
  makeBuyOrder(symbol: string, investment: Money): Promise<BuyOrder> {
    if (symbol === "SPAM")
      throw new AccountSystemError("Insufficient investment to buy an stock.");
    if (symbol === "INVALID")
      throw new AccountSystemError("Invalid symbol provided.");
    if (symbol === "GALI.XBUE") {
      const value = 23;
      const quantity = Math.floor(investment.amount / value);
      return Promise.resolve(new BuyOrder(new ARS(quantity * value), quantity));
    }
    if (symbol === "HAVA.XBUE") {
      const value = 17;
      const quantity = Math.floor(investment.amount / value);
      return Promise.resolve(new BuyOrder(new ARS(quantity * value), quantity));
    }
    if (symbol === "SVB") {
      let buyOrder;
      if (this.svbCalls % 2 === 0) {
        const value = 23;
        const quantity = Math.floor(investment.amount / value);
        buyOrder = new BuyOrder(new ARS(quantity * value), quantity);
        this.svbCalls++;
      } else {
        const value = 17;
        const quantity = Math.floor(investment.amount / value);
        buyOrder = new BuyOrder(new ARS(quantity * value), quantity);
        this.svbCalls++;
      }
      return Promise.resolve(buyOrder);
    }
    return Promise.resolve(new BuyOrder(investment, 1));
  }
  getMarketValue(_symbol: string): Promise<FinancialInstrumentValue | null> {
    throw new Error("Method not implemented.");
  }
}
describe("AccountSystem", () => {
  const nowDate = new Date("2022-10-19T15:30:00.000Z");
  const clock = new TestClock(nowDate);
  let sut: AccountSystem;

  beforeEach(() => {
    sut = new AccountSystem(new TestLedger(clock), new TestBroker());
  });

  describe("getAccount", () => {
    it("A non existent account should throw error", async () => {
      const accountId = 1111;

      await expect(() => sut.getAccount(accountId)).rejects.toThrow(
        "No account with id."
      );
    });
  });

  describe("account creation", () => {
    it("A new account is created with balance and valuation at 0", async () => {
      const userId = "2123";
      const account = await sut.createAccountFor(userId);

      expect(account.id).toBeNumber();
      expect(account).toEqual(
        new Account(account.id, userId, clock.now(), ARS.Zero(), ARS.Zero())
      );
    });

    it("An user can have only one account", async () => {
      const userId = "2123";
      await sut.createAccountFor(userId);
      await expect(() => sut.createAccountFor(userId)).rejects.toThrow(
        "User already have an account."
      );
    });

    it("User id is requered to create an account", async () => {
      // No es posible probar por Typescript que userId se null
      // const userId = null;
      // const nowDate = new Date("2022-10-19T15:30:00.000Z");
      // const clock = new TestClock(nowDate);
      // const sut = new AccountSystem(new TestLedger(clock), new TestBroker());
      // const account = await sut.createAccountFor(userId);

      // expect(account.id).toBeNumber();
      // expect(account).toEqual(
      //   new Account(account.id, userId, clock.now(), ARS.Zero(), ARS.Zero())
      // );
      expect(true).toBeTrue();
    });

    it("User id cannot be empty string", async () => {
      // NOTE este test no estaba originalmente
      const userId = "";
      await expect(() => sut.createAccountFor(userId)).rejects.toThrow(
        "UserId cannot be an empty."
      );
    });

    it("Two differents users create different accounts", async () => {
      const userId = "2123";
      const anotherUserId = "3212";

      const anAccount = await sut.createAccountFor(userId);

      clock.advance(100);

      const anotherAccount = await sut.createAccountFor(anotherUserId);

      expect(anAccount.id).not.toBe(anotherAccount.id);
      expect(anotherAccount.creationDate).toBeAfter(anAccount.creationDate);
    });
  });

  describe("makeDepositFor", () => {
    let anAccount: Account;
    const userId = "2123";

    beforeEach(async () => {
      anAccount = await sut.createAccountFor(userId);
    });

    it("A positive deposit will generate a transaction and add cash balance to the account", async () => {
      const deposit = new ARS(100);
      const transaction = await sut.makeDepositFor(anAccount.id, deposit);
      expect(transaction.id).toBeString();
      expect(anAccount.cashBalance.equals(deposit)).toBeTrue();
    });

    it("Two positive deposits will generate two different transactions and add the sum of amount to cash balance of the account", async () => {
      const deposit = new ARS(100);
      const expectedCashBalance = new ARS(200);

      const firstTransaction = await sut.makeDepositFor(anAccount.id, deposit);
      const secondTransaction = await sut.makeDepositFor(anAccount.id, deposit);

      expect(firstTransaction.id).toBeString();
      expect(secondTransaction.id).toBeString();
      expect(secondTransaction.id).not.toEqual(firstTransaction.id);
      expect(anAccount.cashBalance.equals(expectedCashBalance)).toBeTrue();
    });

    it("A deposit of zero will throw an error", async () => {
      const deposit = new ARS(0);

      // NOTE originalmente no lanzaba un error
      await expect(() =>
        sut.makeDepositFor(anAccount.id, deposit)
      ).rejects.toThrow("Deposits must be for an amount greater than zero.");
    });

    it("A negative deposit will generate a bad request", async () => {
      const deposit = new ARS(-0.1);

      // NOTE originalmente no lanzaba un error
      await expect(() =>
        sut.makeDepositFor(anAccount.id, deposit)
      ).rejects.toThrow("Deposits must be for an amount greater than zero.");
    });

    it("Only deposits in ARS", async () => {
      const deposit = new USD(100);

      await expect(() =>
        sut.makeDepositFor(anAccount.id, deposit)
      ).rejects.toThrow("Only ARS deposits are accepted.");
    });
  });

  describe("makeWithdrawFor", () => {
    let anAccount: Account;
    const userId = "2123";

    beforeEach(async () => {
      anAccount = await sut.createAccountFor(userId);
    });

    it("Cannot withdraw if cash balance is zero", async () => {
      const withdraw = new ARS(0.1);

      await expect(() =>
        sut.makeWithdrawFor(anAccount.id, withdraw)
      ).rejects.toThrow("Insufficient funds to make the withdrawal.");
    });

    it("Cannot withdraw more than cash balance", async () => {
      const deposit = new ARS(100);
      const withdraw = new ARS(100.01);

      await sut.makeDepositFor(anAccount.id, deposit);

      expect(anAccount.cashBalance.equals(deposit)).toBeTrue();
      await expect(() =>
        sut.makeWithdrawFor(anAccount.id, withdraw)
      ).rejects.toThrow("Insufficient funds to make the withdrawal.");
    });

    it("Only withdraws in ARS", async () => {
      const withdraw = new USD(100);

      await expect(() =>
        sut.makeWithdrawFor(anAccount.id, withdraw)
      ).rejects.toThrow("Only ARS withdrawals are accepted.");
    });

    it("Can withdraw if amount is equal to cash balance", async () => {
      const amount = new ARS(100);

      const depositTransaction = await sut.makeDepositFor(anAccount.id, amount);
      const withdrawTransaction = await sut.makeWithdrawFor(
        anAccount.id,
        amount
      );

      expect(depositTransaction.id).not.toEqual(withdrawTransaction.id);
      expect(anAccount.cashBalance.equals(ARS.Zero())).toBeTrue();
    });

    it("Can withdraw if amount is less than cash balance", async () => {
      const deposit = new ARS(100);
      const withdraw = new ARS(99.99);
      const expectedBalance = deposit.substract(withdraw);

      const depositTransaction = await sut.makeDepositFor(
        anAccount.id,
        deposit
      );
      const withdrawTransaction = await sut.makeWithdrawFor(
        anAccount.id,
        withdraw
      );

      expect(depositTransaction.id).not.toEqual(withdrawTransaction.id);
      expect(anAccount.cashBalance.equals(expectedBalance)).toBeTrue();
    });
  });

  describe("makeBuyOrder", () => {
    let anAccount: Account;
    const userId = "2123";
    const galiXBueSymbol = "GALI.XBUE";
    const havaXBueSymbol = "HAVA.XBUE";
    const svbSymbol = "SVB";

    beforeEach(async () => {
      anAccount = await sut.createAccountFor(userId);
    });

    it("Cannot buy a ticker if cash balance is 0", async () => {
      const amountToInvest = new ARS(100);

      await expect(() =>
        sut.makeBuyOrder(anAccount.id, "YPF", amountToInvest)
      ).rejects.toThrow("Insufficient funds to make buy order.");
    });

    it("Cannot buy a ticker if cash balance is less than real invesment", async () => {
      const deposit = new ARS(91.99);
      const amountToInvest = new ARS(100);
      sut.makeDepositFor(anAccount.id, deposit);

      await expect(() =>
        sut.makeBuyOrder(anAccount.id, "YPF", amountToInvest)
      ).rejects.toThrow("Insufficient funds to make buy order.");
    });

    it("Cannot buy a ticker if higher than investment amount", async () => {
      const deposit = new ARS(100);
      const amountToInvest = new ARS(50);
      sut.makeDepositFor(anAccount.id, deposit);

      await expect(() =>
        sut.makeBuyOrder(anAccount.id, "SPAM", amountToInvest)
      ).rejects.toThrow("Insufficient investment to buy an stock.");
    });

    it("Cannot buy a ticker if investment currency are not ARS", async () => {
      const deposit = new ARS(300);
      const amountToInvest = new USD(1);
      sut.makeDepositFor(anAccount.id, deposit);

      await expect(() =>
        sut.makeBuyOrder(anAccount.id, "YPF", amountToInvest)
      ).rejects.toThrow("Only ARS investments are accepted.");
    });

    it("Cannot buy an invalid ticker", async () => {
      const deposit = new ARS(100);
      const amountToInvest = new ARS(50);
      sut.makeDepositFor(anAccount.id, deposit);

      await expect(() =>
        sut.makeBuyOrder(anAccount.id, "INVALID", amountToInvest)
      ).rejects.toThrow("Invalid symbol provided.");
    });

    it("Buy stocks near investment", async () => {
      const deposit = new ARS(100);
      const amountToInvest = new ARS(100);
      await sut.makeDepositFor(anAccount.id, deposit);

      const buyOrder = await sut.makeBuyOrder(
        anAccount.id,
        galiXBueSymbol,
        amountToInvest
      );

      expect(buyOrder).toEqual({
        transactionId: expect.stringMatching(
          /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
        ),
        symbol: galiXBueSymbol,
        buyedStock: 4,
        realInvestment: new ARS(92.0),
      });
      expect(anAccount.cashBalance).toEqual(new ARS(8.0));
      // TODO deberia verificar las inversiones de la cuenta
    });

    // In integration is named: 'two symbols with different prices buy'
    it("Buy two different stocks with different prices", async () => {
      const deposit = new ARS(100);
      const amountToInvest = new ARS(50);
      await sut.makeDepositFor(anAccount.id, deposit);

      const galiBuyOrder = await sut.makeBuyOrder(
        anAccount.id,
        galiXBueSymbol,
        amountToInvest
      );

      const havaBuyOrder = await sut.makeBuyOrder(
        anAccount.id,
        havaXBueSymbol,
        amountToInvest
      );

      expect(galiBuyOrder).toEqual({
        transactionId: expect.stringMatching(
          /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
        ),
        symbol: galiXBueSymbol,
        buyedStock: 2,
        realInvestment: new ARS(46.0),
      });
      expect(havaBuyOrder).toEqual({
        transactionId: expect.stringMatching(
          /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
        ),
        symbol: havaXBueSymbol,
        buyedStock: 2,
        realInvestment: new ARS(34.0),
      });
      expect(anAccount.cashBalance).toEqual(new ARS(20.0));
      // TODO deberia verificar las inversiones de la cuenta
    });

    // In integration is named: 'same symbol with different prices'
    it("Buy same stock and get different prices", async () => {
      const deposit = new ARS(100);
      const amountToInvest = new ARS(50);
      await sut.makeDepositFor(anAccount.id, deposit);

      const firstBuyOrder = await sut.makeBuyOrder(
        anAccount.id,
        svbSymbol,
        amountToInvest
      );

      const secondBuyOrder = await sut.makeBuyOrder(
        anAccount.id,
        svbSymbol,
        amountToInvest
      );

      expect(firstBuyOrder).toEqual({
        transactionId: expect.stringMatching(
          /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
        ),
        symbol: svbSymbol,
        buyedStock: 2,
        realInvestment: new ARS(46.0),
      });
      expect(secondBuyOrder).toEqual({
        transactionId: expect.stringMatching(
          /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
        ),
        symbol: svbSymbol,
        buyedStock: 2,
        realInvestment: new ARS(34.0),
      });
      expect(anAccount.cashBalance).toEqual(new ARS(20.0));
      // TODO deberia verificar las inversiones de la cuenta
    });
  });

  describe("makeSellOrder", () => {
    // TODO Hay que hacer el feature
  });
});
