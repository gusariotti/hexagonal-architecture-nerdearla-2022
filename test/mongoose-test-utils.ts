import type { DeleteResult } from "mongodb";
import type { Connection } from "mongoose";

export async function removeAllCollections(mongooseConnection: Connection) {
  const collections = Object.keys(mongooseConnection.collections);
  const deletations: Promise<DeleteResult>[] = [];
  collections.forEach((collectionName) => {
    const collection = mongooseConnection.collections[collectionName];
    if (collection != null) deletations.push(collection.deleteMany({}));
  });
  return Promise.all(deletations);
}

export async function dropAllCollections(mongooseConnection: Connection) {
  const collections = Object.keys(mongooseConnection.collections);
  const drops: Promise<boolean>[] = [];
  collections.forEach((collectionName) => {
    const collection = mongooseConnection.collections[collectionName];
    try {
      if (collection != null) drops.push(collection.drop());
    } catch (error) {
      if (error instanceof Error) {
        // Sometimes this error happens, but you can safely ignore it
        if (error.message === "ns not found") return;
        // This error occurs when you use it.todo. You can
        // safely ignore this error too
        if (
          error.message.includes("a background operation is currently running")
        )
          return;
        console.log(error.message);
      } else {
        console.log(
          `Error adding ${collectionName} drop call`,
          JSON.stringify(error)
        );
      }
    }
  });
  return Promise.all(drops);
}
