import { connect, connection } from "mongoose";
import { buildUsing } from "../src/app";
import {
  removeAllCollections,
  dropAllCollections,
} from "./mongoose-test-utils";
import type { FastifyInstanceWithTypeBox } from "../src/shared/types";
import { AppOptions, MongooseOptions } from "../src/shared/appOptions";
import got from "got";
import {
  aDepositResponseWithATransactionId,
  anObjectContainingBuyOrderResult,
  anObjectContainingCashBalanceEqual,
  arsCloseTo,
  buyOrderBody,
  createAccount,
  createGotHttpError,
  depositRequestBody,
  getAccount,
  gotMockedReturnValue,
  gotMockGetResponseWith,
  makeADeposit,
  makeBuyOrder,
  makeWithdraw,
  marketSymbolValueFakeResponse,
  TestClock,
  withdrawRequestBody,
} from "./test-utils";
import "./jest-utils";
import { ARS, USD } from "../src/shared/money";
import type {
  CreateAccountRequestBodyType,
  GetAccountResponseType,
} from "../src/accounts/accountTypes";
import { AccountSystem } from "../src/accounts/accountSystem";
import { BrokerMarketStack } from "../src/accounts/broker";
import { MongooseLedger } from "../src/accounts/ledger";
import type { Clock } from "../src/shared/clock";
jest.mock("got");

jest.setTimeout(100000);

describe("Accounts App", () => {
  let clock: TestClock;
  let app: FastifyInstanceWithTypeBox;
  const nowDate = new Date("2022-10-19T15:30:00.000Z");
  const anAccountCreationRequestBody: CreateAccountRequestBodyType = {
    userId: "2123",
  };

  async function appForTest(clock: Clock) {
    const fastifyOptions = {};
    const mongooseOptions: MongooseOptions = {
      connection: connection,
      forceClose: true,
    };
    const accounts = new MongooseLedger(clock);
    const broker = new BrokerMarketStack("http://localhost/", "");
    const system = new AccountSystem(accounts, broker);
    const options: AppOptions = new AppOptions(
      clock,
      system,
      fastifyOptions,
      mongooseOptions
    );
    const app = buildUsing(options);
    await app.ready();
    return app;
  }

  async function createInitialAccount(app: FastifyInstanceWithTypeBox) {
    return await createAccount(app, anAccountCreationRequestBody);
  }

  beforeAll(async () => {
    const connectionString = process.env["MONGO_URL"];
    if (connectionString == null)
      throw new Error("Process env MONGO_URL undefined");
    await connect(connectionString, {});

    clock = new TestClock(nowDate);
    app = await appForTest(clock);
  });

  beforeEach(() => {
    clock.nowDate = nowDate;
  });

  afterEach(async () => {
    await removeAllCollections(connection);
  });

  // Disconnect Mongoose
  afterAll(async () => {
    await dropAllCollections(connection);
    await connection.close();
  });

  describe("account get", () => {
    it("A non existent account should throw error", async () => {
      const getAccountResponse = await getAccount(app, {
        body: { accountId: 1111 },
      });
      expect(getAccountResponse).toBeABadRequestWithMessage(
        "No account with id."
      );
    });
  });
  describe("account creation", () => {
    it("A new account is created with balance and valuation at 0", async () => {
      const createAccountResponse = await createInitialAccount(app);

      const expectedCreateAccountResponse = {
        userId: anAccountCreationRequestBody.userId,
        accountId: expect.toBeNumber(),
      };
      expect(createAccountResponse).toBeAnOkResponseWithBody(
        expectedCreateAccountResponse
      );

      const expectedGetAccountResponse: GetAccountResponseType = {
        accountId: createAccountResponse.body.accountId,
        userId: createAccountResponse.body.userId,
        creationDate: clock.now().toISOString(),
        cashBalance: ARS.Zero(),
        valuation: ARS.Zero(),
      };

      const getAccountResponse = await getAccount(app, createAccountResponse);
      expect(getAccountResponse).toBeAnOkResponseWithBody(
        expectedGetAccountResponse
      );
    });

    it("An user can have only one account", async () => {
      await createInitialAccount(app);

      const secondCreateAccountResponse = await createAccount(
        app,
        anAccountCreationRequestBody
      );
      expect(secondCreateAccountResponse).toBeABadRequestWithMessage(
        "User already have an account."
      );
    });

    it("Two differents users create different accounts", async () => {
      const anotherAccountCreationRequestBody = { userId: "2124" };
      const firstCreateAccountResponse = await createInitialAccount(app);

      clock.advance(100);

      const secondCreateAccountResponse = await createAccount(
        app,
        anotherAccountCreationRequestBody
      );

      const expectedCreateAccountResponse = {
        userId: anotherAccountCreationRequestBody.userId,
        accountId: expect.toBeNumber(),
      };

      expect(secondCreateAccountResponse).toBeAnOkResponseWithBody(
        expectedCreateAccountResponse
      );
      expect(secondCreateAccountResponse.body.accountId).not.toBe(
        firstCreateAccountResponse.body.accountId
      );

      const getFirstAccountResponse = await getAccount(
        app,
        firstCreateAccountResponse
      );
      const getSecondAccountResponse = await getAccount(
        app,
        secondCreateAccountResponse
      );

      expect(getFirstAccountResponse.statusCode).toEqual(200);
      expect(getSecondAccountResponse.statusCode).toEqual(200);
      expect(
        Date.parse(getSecondAccountResponse.body.creationDate)
      ).toBeGreaterThan(Date.parse(getFirstAccountResponse.body.creationDate));
    });

    it("User id is requered to create an account", async () => {
      const user = {};
      const firstCreateAccountResponse = await createAccount(app, user);
      expect(firstCreateAccountResponse.statusCode).toEqual(400);
    });
  });

  describe("deposit", () => {
    let account: any;

    beforeEach(async () => {
      account = await createInitialAccount(app);
    });

    it("A positive deposit will generate a transaction and add cash balance to the account", async () => {
      const depositRequest = depositRequestBody(100, account);

      const depositResponse = await makeADeposit(app, depositRequest);

      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const getAccountResponse = await getAccount(app, account);
      expect(getAccountResponse).toBeAnOkResponseWithBody(
        anObjectContainingCashBalanceEqual(depositRequest.deposit)
      );
    });

    it("Two positive deposits will generate two different transactions and add the sum of amount to cash balance of the account", async () => {
      const depositRequest = depositRequestBody(100, account);
      const expectedCashBalance = new ARS(200);

      const firstDepositResponse = await makeADeposit(app, depositRequest);
      const secondDepositResponse = await makeADeposit(app, depositRequest);

      expect(firstDepositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );
      expect(secondDepositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      expect(firstDepositResponse.body.transactionId).not.toBe(
        secondDepositResponse.body.transactionId
      );

      const getAccountResponse = await getAccount(app, account);
      expect(getAccountResponse).toBeAnOkResponseWithBody(
        anObjectContainingCashBalanceEqual(expectedCashBalance)
      );
    });

    it("A deposit of zero will generate a bad request", async () => {
      const depositRequest = depositRequestBody(0, account);

      const depositResponse = await makeADeposit(app, depositRequest);

      expect(depositResponse.statusCode).toEqual(400);
    });

    it("A negative deposit will generate a bad request", async () => {
      const depositRequest = depositRequestBody(-0.1, account);

      const depositResponse = await makeADeposit(app, depositRequest);

      expect(depositResponse.statusCode).toEqual(400);
    });

    it("Only deposits in ARS", async () => {
      const deposit = new USD(100);

      const depositRequest = {
        accountId: account.body.accountId,
        deposit: deposit,
      };

      const depositResponse = await makeADeposit(app, depositRequest);

      expect(depositResponse).toBeABadRequestWithMessage(
        "Only ARS deposits are accepted."
      );
    });
  });

  describe("withdraw", () => {
    let account: any;

    beforeEach(async () => {
      account = await createInitialAccount(app);
    });

    it("Cannot withdraw if cash balance is zero", async () => {
      const withdrawRequest = withdrawRequestBody(100, account);

      const withdrawResponse = await makeWithdraw(app, withdrawRequest);

      expect(withdrawResponse).toBeABadRequestWithMessage(
        "Insufficient funds to make the withdrawal."
      );
    });

    it("Cannot withdraw more than cash balance", async () => {
      const depositRequest = depositRequestBody(100, account);

      const depositResponse = await makeADeposit(app, depositRequest);
      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const withdrawRequest = withdrawRequestBody(100.01, account);
      const withdrawResponse = await makeWithdraw(app, withdrawRequest);

      expect(withdrawResponse).toBeABadRequestWithMessage(
        "Insufficient funds to make the withdrawal."
      );
    });

    it("Only withdraws in ARS", async () => {
      const withdraw = new USD(100);

      const withdrawRequest = {
        accountId: account.body.accountId,
        withdraw: withdraw,
      };

      const withdrawResponse = await makeWithdraw(app, withdrawRequest);

      expect(withdrawResponse).toBeABadRequestWithMessage(
        "Only ARS withdrawals are accepted."
      );
    });

    it("Can withdraw if amount is equal to cash balance", async () => {
      const depositRequest = depositRequestBody(100, account);

      const depositResponse = await makeADeposit(app, depositRequest);
      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const withdrawRequest = withdrawRequestBody(100, account);
      const withdrawResponse = await makeWithdraw(app, withdrawRequest);
      expect(withdrawResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const getAccountResponse = await getAccount(app, account);
      expect(getAccountResponse).toBeAnOkResponseWithBody(
        anObjectContainingCashBalanceEqual(ARS.Zero())
      );
    });

    it("Can withdraw if amount is less than cash balance", async () => {
      const expectedCashBalance = {
        currency: "ARS",
        amount: expect.closeTo(0.01, 2),
      };

      const depositRequest = depositRequestBody(100, account);

      const depositResponse = await makeADeposit(app, depositRequest);
      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const withdrawRequest = withdrawRequestBody(99.99, account);
      const withdrawResponse = await makeWithdraw(app, withdrawRequest);
      expect(withdrawResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const getAccountResponse = await getAccount(app, account);
      expect(getAccountResponse).toBeAnOkResponseWithBody(
        expect.objectContaining({ cashBalance: expectedCashBalance })
      );
    });
  });

  describe("buy ticker", () => {
    let account: any;
    const galiXbueSymbol = "GALI.XBUE";
    const galiXbueFakeResponse = marketSymbolValueFakeResponse(
      23,
      23,
      galiXbueSymbol
    );

    beforeEach(async () => {
      account = await createInitialAccount(app);
    });

    it("Cannot buy a ticker if cash balance is 0", async () => {
      gotMockGetResponseWith(galiXbueFakeResponse);
      const buyOrder = buyOrderBody(account, galiXbueSymbol, 100);

      const buyOrderResponse = await makeBuyOrder(app, buyOrder);

      expect(buyOrderResponse).toBeABadRequestWithMessage(
        "Insufficient funds to make buy order."
      );
    });

    it("Cannot buy a ticker if cash balance is less than real invesment", async () => {
      gotMockGetResponseWith(galiXbueFakeResponse);

      const depositRequest = depositRequestBody(91.99, account);
      const depositResponse = await makeADeposit(app, depositRequest);
      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const buyOrder = buyOrderBody(account, galiXbueSymbol, 100.0);
      const buyOrderResponse = await makeBuyOrder(app, buyOrder);

      expect(buyOrderResponse).toBeABadRequestWithMessage(
        "Insufficient funds to make buy order."
      );
    });

    it("Cannot buy a ticker if higher than investment amount", async () => {
      gotMockGetResponseWith(galiXbueFakeResponse);

      const depositRequest = depositRequestBody(100, account);
      const depositResponse = await makeADeposit(app, depositRequest);
      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const buyOrder = buyOrderBody(account, galiXbueSymbol, 22.99);

      const buyOrderResponse = await makeBuyOrder(app, buyOrder);

      expect(buyOrderResponse).toBeABadRequestWithMessage(
        "Insufficient investment to buy an stock."
      );
    });

    it("Cannot buy a ticker if investment currency are not ARS", async () => {
      const buyOrder = {
        accountId: account.body.accountId,
        symbol: galiXbueSymbol,
        investment: {
          currency: "USD",
          amount: 22.9,
        },
      };
      const buyOrderResponse = await makeBuyOrder(app, buyOrder);

      expect(buyOrderResponse).toBeABadRequestWithMessage(
        "Only ARS investments are accepted."
      );
    });

    it("Cannot buy an invalid ticker", async () => {
      const errorMessage = "Response code 422 (Unprocessable Entity)";
      const apiResponse = {
        body: {
          error: {
            code: "no_valid_symbols_provided",
            message: "At least one valid symbol must be provided",
          },
        },
      };
      const fakeResponse = createGotHttpError(errorMessage, apiResponse);

      const mockedGot = jest.mocked(got);
      mockedGot.mockReturnValue({
        json: () => {
          throw fakeResponse;
        },
      } as any);

      const buyOrder = buyOrderBody(account, galiXbueSymbol, 100);

      const buyOrderResponse = await makeBuyOrder(app, buyOrder);

      expect(buyOrderResponse).toBeABadRequestWithMessage(
        "Invalid symbol provided."
      );
    });

    it("Buy stocks near investment", async () => {
      gotMockGetResponseWith(galiXbueFakeResponse);
      const depositRequest = depositRequestBody(100, account);
      const depositResponse = await makeADeposit(app, depositRequest);
      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const buyOrder = buyOrderBody(account, galiXbueSymbol, 100);
      const buyOrderResponse = await makeBuyOrder(app, buyOrder);

      expect(buyOrderResponse).toBeAnOkResponseWithBody(
        anObjectContainingBuyOrderResult(galiXbueSymbol, 4, 92.0, 2)
      );

      const getAccountResponse = await getAccount(app, account);
      expect(getAccountResponse).toBeAnOkResponseWithBody(
        expect.objectContaining({ cashBalance: arsCloseTo(8.0, 2) })
      );
    });

    it("two symbols with different prices buy", async () => {
      const havaXBueSymbol = "HAVA.XBUE";
      const havaXbueFakeResponse = marketSymbolValueFakeResponse(
        17,
        17,
        havaXBueSymbol
      );
      const mockedGot = jest.mocked(got);
      mockedGot.mockImplementation((_options, ...args) => {
        if (args.length > 0) {
          const { searchParams } = (args as any[])[0];
          if (searchParams.symbols === havaXBueSymbol)
            return gotMockedReturnValue(havaXbueFakeResponse);
          if (searchParams.symbols === galiXbueSymbol)
            return gotMockedReturnValue(galiXbueFakeResponse);
        }
        throw new Error(`Unknown request!${JSON.stringify(args)}`);
      });

      const depositRequest = depositRequestBody(100, account);
      const depositResponse = await makeADeposit(app, depositRequest);
      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const buyOrderGali = buyOrderBody(account, galiXbueSymbol, 50);
      const buyOrderResponseGali = await makeBuyOrder(app, buyOrderGali);
      expect(buyOrderResponseGali).toBeAnOkResponseWithBody(
        anObjectContainingBuyOrderResult(galiXbueSymbol, 2, 46.0, 2)
      );

      const buyOrderHava = buyOrderBody(account, havaXBueSymbol, 50);
      const buyOrderResponseHava = await makeBuyOrder(app, buyOrderHava);
      expect(buyOrderResponseHava).toBeAnOkResponseWithBody(
        anObjectContainingBuyOrderResult(havaXBueSymbol, 2, 34.0, 2)
      );

      const getAccountResponse = await getAccount(app, account);
      expect(getAccountResponse).toBeAnOkResponseWithBody(
        expect.objectContaining({ cashBalance: arsCloseTo(20.0, 2) })
      );
    });

    it("same symbol with different prices", async () => {
      const galiXbueSecodFakeResponse = marketSymbolValueFakeResponse(
        17,
        17,
        galiXbueSymbol
      );
      const mockedGot = jest.mocked(got);
      mockedGot
        .mockReturnValueOnce(gotMockedReturnValue(galiXbueFakeResponse))
        .mockReturnValueOnce(gotMockedReturnValue(galiXbueSecodFakeResponse));

      const depositRequest = depositRequestBody(100, account);
      const depositResponse = await makeADeposit(app, depositRequest);
      expect(depositResponse).toBeAnOkResponseWithBody(
        aDepositResponseWithATransactionId
      );

      const buyOrderGali = buyOrderBody(account, galiXbueSymbol, 50);
      const firstBuyOrderResponseGali = await makeBuyOrder(app, buyOrderGali);
      expect(firstBuyOrderResponseGali).toBeAnOkResponseWithBody(
        anObjectContainingBuyOrderResult(galiXbueSymbol, 2, 46.0, 2)
      );

      const secondbuyOrderResponseGali = await makeBuyOrder(app, buyOrderGali);
      expect(secondbuyOrderResponseGali).toBeAnOkResponseWithBody(
        anObjectContainingBuyOrderResult(galiXbueSymbol, 2, 34.0, 2)
      );

      const getAccountResponse = await getAccount(app, account);
      expect(getAccountResponse).toBeAnOkResponseWithBody(
        expect.objectContaining({ cashBalance: arsCloseTo(20.0, 2) })
      );
    });
  });
  describe("sell invesment", () => {
    // TODO Hay que hacer el feature
  });
});
