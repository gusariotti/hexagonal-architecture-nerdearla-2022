import type { JestConfigWithTsJest } from "ts-jest/dist/types";
import { defaults as tsjPreset } from "ts-jest/presets";
import { default as jestMongoDBPreset } from "@shelf/jest-mongodb/lib";

// import { defaultsESM as tsjPreset } from 'ts-jest/presets'
// import { jsWithTs as tsjPreset } from 'ts-jest/presets'
// import { jsWithTsESM as tsjPreset } from 'ts-jest/presets'
// import { jsWithBabel as tsjPreset } from 'ts-jest/presets'
// import { jsWithBabelESM as tsjPreset } from 'ts-jest/presets'

const config: JestConfigWithTsJest = {
  ...tsjPreset,
  transform: {
    ...tsjPreset.transform,
    // [...]
  },
  ...jestMongoDBPreset,
  setupFilesAfterEnv: ["jest-extended/all"],
};

export default config;
