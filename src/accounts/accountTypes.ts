import { Static, Type } from "@sinclair/typebox";
import type { RouteGenericInterface } from "fastify";

// Comunes
export const ErrorResponse = Type.Object(
  {
    statusCode: Type.Number(),
    error: Type.String(),
    message: Type.String(),
  },
  { additionalProperties: false }
);

export type ErrorResponseType = Static<typeof ErrorResponse>;

export const UserId = Type.String({ $id: "userId" });
export type UserIdType = Static<typeof UserId>;

export const AccountId = Type.Number();
export type AccountIdType = Static<typeof AccountId>;

const Money = Type.Object(
  {
    currency: Type.String(),
    amount: Type.Number(),
  },
  { additionalProperties: false }
);

// Creacion de cuenta
export const CreateAccountRequestBody = Type.Object(
  {
    userId: UserId,
  },
  { additionalProperties: false }
);
export type CreateAccountRequestBodyType = Static<
  typeof CreateAccountRequestBody
>;

export const CreateAccountSuccessResponse = Type.Object(
  {
    userId: UserId,
    accountId: AccountId,
  },
  { additionalProperties: false }
);
export type CreateAccountSuccessResponseType = Static<
  typeof CreateAccountSuccessResponse
>;
const CreateAccountResponse = Type.Union([
  CreateAccountSuccessResponse,
  ErrorResponse,
]);
export type CreateAccountResponseType = Static<typeof CreateAccountResponse>;

// Obtener cuenta
export const GetAccountParams = Type.Object({ accountId: Type.Number() });
export type GetAccountParamsType = Static<typeof GetAccountParams>;

export const GetAccountSuccessResponse = Type.Object(
  {
    userId: UserId,
    accountId: AccountId,
    creationDate: Type.String(),
    cashBalance: Money,
    valuation: Money,
  },
  { additionalProperties: false }
);
export type GetAccountSuccessResponseType = Static<
  typeof GetAccountSuccessResponse
>;
const GetAccountResponse = Type.Union([
  GetAccountSuccessResponse,
  ErrorResponse,
]);
export type GetAccountResponseType = Static<typeof GetAccountResponse>;

// Depositar
const Deposit = Type.Object(
  {
    currency: Type.String(),
    amount: Type.Number({ exclusiveMinimum: 0 }),
  },
  { additionalProperties: false }
);

export const DepositRequest = Type.Object(
  {
    accountId: Type.Number(),
    deposit: Deposit,
  },
  { additionalProperties: false }
);

export type DepositRequestType = Static<typeof DepositRequest>;

export const DepositSuccessResponse = Type.Object(
  {
    transactionId: Type.String(),
  },
  { additionalProperties: false }
);

export type DepositSuccessResponse = Static<typeof DepositSuccessResponse>;

const DepositResponse = Type.Union([DepositSuccessResponse, ErrorResponse]);
export type DepositResponseType = Static<typeof DepositResponse>;

const Withdraw = Type.Object(
  {
    currency: Type.String(),
    amount: Type.Number({ exclusiveMinimum: 0 }),
  },
  { additionalProperties: false }
);

export const WithdrawRequest = Type.Object(
  {
    accountId: Type.Number(),
    withdraw: Withdraw,
  },
  { additionalProperties: false }
);

// Retirar
export type WithdrawRequestType = Static<typeof WithdrawRequest>;

export const WithdrawSuccessResponse = Type.Object(
  {
    transactionId: Type.String(),
  },
  { additionalProperties: false }
);

export type WithdrawSuccessResponseType = Static<
  typeof WithdrawSuccessResponse
>;

const WithdrawResponse = Type.Union([WithdrawSuccessResponse, ErrorResponse]);
export type WithdrawResponseType = Static<typeof WithdrawResponse>;

// Comprar
const Investment = Type.Object(
  {
    currency: Type.String(),
    amount: Type.Number({ exclusiveMinimum: 0 }),
  },
  { additionalProperties: false }
);

export const BuyRequest = Type.Object(
  {
    accountId: Type.Number(),
    symbol: Type.String(),
    investment: Investment,
  },
  { additionalProperties: false }
);

export type BuyRequestType = Static<typeof BuyRequest>;

export const BuySuccessResponse = Type.Object(
  {
    transactionId: Type.String(),
    symbol: Type.String(),
    buyedStock: Type.Number({ exclusiveMinimum: 0 }),
    realInvestment: Investment,
  },
  { additionalProperties: false }
);

export type BuySuccessResponseType = Static<typeof BuySuccessResponse>;

const BuyResponse = Type.Union([BuySuccessResponse, ErrorResponse]);
export type BuyResponseType = Static<typeof BuyResponse>;

//Tipos para las rutas y handlers de rutas de Fastify
export interface CreateAccountType extends RouteGenericInterface {
  Body: CreateAccountRequestBodyType;
  Reply: CreateAccountResponseType;
}
export interface GetAccountType extends RouteGenericInterface {
  Params: GetAccountParamsType;
  Reply: GetAccountResponseType;
}
export interface DepositType extends RouteGenericInterface {
  Body: DepositRequestType;
  Reply: DepositResponseType;
}
export interface WithdrawType extends RouteGenericInterface {
  Body: WithdrawRequestType;
  Reply: WithdrawResponseType;
}
export interface BuyType extends RouteGenericInterface {
  Body: BuyRequestType;
  Reply: BuyResponseType;
}
