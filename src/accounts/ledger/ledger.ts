import type { Account } from "../account";
import { v4 as uuidv4 } from "uuid";

// TODO Falta conceptualizar completo y persistir
export class Transaction {
  readonly id: string;

  static createWithUUIDv4Id() {
    return new Transaction(uuidv4());
  }

  constructor(id: string) {
    this.id = id;
  }
}

export abstract class Ledger {
  abstract findAccountWith(id: number): Promise<Account | null>;
  abstract createAccountFor(userId: string): Promise<Account>;
  abstract registerCashBalance(account: Account): Promise<Transaction>;
}
