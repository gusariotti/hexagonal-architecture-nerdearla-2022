import { Schema, model, connection } from "mongoose";
import type { MongoServerError } from "mongodb";
import { AccountSystemError } from "../accountErrors";
import type { Clock } from "../../shared/clock";
import { Account, AccountLike } from "../account";
import { Ledger, Transaction } from "./ledger";

// mongoose-sequence no tiene @types/mongoose-sequence al dia
// eslint-disable-next-line @typescript-eslint/no-var-requires
const AutoIncrementFactory = require("mongoose-sequence");

const AccountMongooseSchema = new Schema<AccountLike>({
  id: {
    type: Number,
    unique: true,
    index: true,
  },
  userId: { type: String, required: true, unique: true },
  creationDate: { type: String, required: true },
  cashBalance: { type: Object, required: true },
  valuation: { type: Object, required: true },
});
const AutoIncrement = AutoIncrementFactory(connection);
AccountMongooseSchema.plugin(AutoIncrement, { inc_field: "id" });

export const AccountModel = model<AccountLike>(
  "Account",
  AccountMongooseSchema
);
export function isMongoServerError(error: unknown): error is MongoServerError {
  return (
    typeof error === "object" &&
    error != null &&
    error.constructor.name === "MongoServerError"
  );
}
export function isDuplicateUserIdError(error: MongoServerError): boolean {
  return (
    error?.code === 11000 &&
    "keyValue" in error &&
    "userId" in error["keyValue"]
  );
}

export class MongooseLedger implements Ledger {
  clock: Clock;

  constructor(clock: Clock) {
    this.clock = clock;
  }

  async findAccountWith(id: number): Promise<Account | null> {
    const accountLike = await AccountModel.findOne(
      { id },
      "id userId creationDate cashBalance valuation"
    )
      .lean()
      .exec();
    if (accountLike != null) return Account.fromAccountLike(accountLike);
    return null;
  }

  async createAccountFor(userId: string): Promise<Account> {
    try {
      const account = {
        userId: userId,
        creationDate: this.clock.now().toISOString(),
        cashBalance: {
          currency: "ARS",
          amount: 0,
        },
        valuation: {
          currency: "ARS",
          amount: 0,
        },
      };
      const accountDoc = new AccountModel(account);
      await accountDoc.save();

      return Account.fromAccountLike(accountDoc);
    } catch (error) {
      if (isMongoServerError(error) && isDuplicateUserIdError(error)) {
        throw new AccountSystemError("User already have an account.");
      } else {
        throw error;
      }
    }
  }

  async registerCashBalance(account: Account) {
    const result = await AccountModel.updateOne(
      { id: account.id },
      { cashBalance: account.cashBalance }
    ).exec();
    if (result.matchedCount === 1 && result.modifiedCount === 1) {
      return Transaction.createWithUUIDv4Id();
    }
    throw new AccountSystemError(
      `ERROR! Deposit persistence failed. Account id: ${account.id}`
    );
  }
}
