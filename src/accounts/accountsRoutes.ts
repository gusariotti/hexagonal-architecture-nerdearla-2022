import type { FastifySchema } from "fastify";
import type { AppOptions } from "../shared/appOptions";
import type {
  FastifyInstanceWithTypeBox,
  RouteOptionsWithTypeBox,
} from "../shared/types";
import { AccountController } from "./accountController";
import {
  CreateAccountType,
  DepositType,
  GetAccountType,
  CreateAccountRequestBody,
  CreateAccountSuccessResponse,
  ErrorResponse,
  GetAccountParams,
  GetAccountSuccessResponse,
  DepositSuccessResponse,
  DepositRequest,
  WithdrawRequest,
  WithdrawSuccessResponse,
  WithdrawType,
  BuyRequest,
  BuySuccessResponse,
  BuyType,
} from "./accountTypes";

export class AccountRoutes {
  controller: AccountController;

  static using(options: AppOptions): AccountRoutes {
    const accountController = AccountController.using(options);
    return new AccountRoutes(accountController);
  }

  constructor(anAccountController: AccountController) {
    this.controller = anAccountController;
  }

  addCreateAccountRoute(app: FastifyInstanceWithTypeBox) {
    const createAccountSchema: FastifySchema = {
      body: CreateAccountRequestBody,
      response: {
        200: CreateAccountSuccessResponse,
        400: ErrorResponse,
      },
    };
    const route: RouteOptionsWithTypeBox<CreateAccountType> = {
      method: "POST",
      url: "/account",
      schema: createAccountSchema,
      handler: this.controller.createAccount.bind(this.controller),
    };
    app.route(route);
  }

  addGetAccountRoute(app: FastifyInstanceWithTypeBox) {
    const getAccountSchema: FastifySchema = {
      params: GetAccountParams,
      response: {
        200: GetAccountSuccessResponse,
        400: ErrorResponse,
      },
    };

    const route: RouteOptionsWithTypeBox<GetAccountType> = {
      method: "GET",
      url: "/account/:accountId",
      schema: getAccountSchema,
      handler: this.controller.getAccount.bind(this.controller),
    };
    app.route(route);
  }

  addDepositRoute(app: FastifyInstanceWithTypeBox) {
    const depositSchema: FastifySchema = {
      body: DepositRequest,
      response: {
        200: DepositSuccessResponse,
        400: ErrorResponse,
      },
    };

    const route: RouteOptionsWithTypeBox<DepositType> = {
      method: "POST",
      url: "/account/deposit",
      schema: depositSchema,
      handler: this.controller.deposit.bind(this.controller),
    };
    app.route(route);
  }

  addWithdrawRoute(app: FastifyInstanceWithTypeBox) {
    const withdrawSchema: FastifySchema = {
      body: WithdrawRequest,
      response: {
        200: WithdrawSuccessResponse,
        400: ErrorResponse,
      },
    };

    const route: RouteOptionsWithTypeBox<WithdrawType> = {
      method: "POST",
      url: "/account/withdraw",
      schema: withdrawSchema,
      handler: this.controller.withdraw.bind(this.controller),
    };
    app.route(route);
  }

  addBuyRoute(app: FastifyInstanceWithTypeBox) {
    // TODO crear clase para generacion de esquemas
    // NOTE esquema de validacion de input y output de Fastify
    const BuySchema: FastifySchema = {
      body: BuyRequest,
      response: {
        200: BuySuccessResponse,
        400: ErrorResponse,
      },
    };

    // TODO crear clase para generacion de rutas
    // NOTE definifcion de la ruta con su tipo para inferir los tipos del handler
    const route: RouteOptionsWithTypeBox<BuyType> = {
      method: "POST",
      url: "/account/buy",
      schema: BuySchema,
      handler: this.controller.buy.bind(this.controller),
    };
    app.route(route);
  }

  routes() {
    return (app: FastifyInstanceWithTypeBox, _options: any, done: any) => {
      //type PostType = { Body: AccountPostBodyType, Reply: AccountType };
      this.addCreateAccountRoute(app);
      this.addGetAccountRoute(app);
      this.addDepositRoute(app);
      this.addWithdrawRoute(app);
      this.addBuyRoute(app);
      // this.addSellRoute(app);
      done();
    };
  }
}
