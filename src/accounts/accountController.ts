import type {
  FastifyReplyWithTypeBox,
  FastifyRequestWithTypeBox,
} from "../shared/types";
import {
  BuyType,
  CreateAccountType,
  DepositType,
  GetAccountSuccessResponse,
  GetAccountType,
  WithdrawType,
} from "./accountTypes";
import { Value } from "@sinclair/typebox/value";
import type { AppOptions } from "../shared/appOptions";
import { Money } from "../shared/money";
import type { AccountSystem } from "./accountSystem";

export class AccountController {
  private readonly system;
  static using(options: AppOptions) {
    return new AccountController(options.accountSystem);
  }

  constructor(system: AccountSystem) {
    this.system = system;
  }

  async getAccount(
    request: FastifyRequestWithTypeBox<GetAccountType>,
    reply: FastifyReplyWithTypeBox<GetAccountType>
  ): Promise<void> {
    const { accountId } = request.params;
    const system = this.system;
    const account = await system.getAccount(accountId);
    // NOTE la transformacion deberia encapsularse
    const response = Value.Cast(GetAccountSuccessResponse, account);
    response.accountId = account.id;
    response.creationDate = account.creationDate.toISOString();
    reply.status(200).send(response);
  }

  async createAccount(
    request: FastifyRequestWithTypeBox<CreateAccountType>,
    reply: FastifyReplyWithTypeBox<CreateAccountType>
  ): Promise<void> {
    const { userId } = request.body;
    const account = await this.system.createAccountFor(userId);
    return reply.status(200).send({ userId, accountId: account.id });
  }

  async deposit(
    request: FastifyRequestWithTypeBox<DepositType>,
    reply: FastifyReplyWithTypeBox<DepositType>
  ): Promise<void> {
    const { accountId, deposit } = request.body;
    const moneyDeposit = Money.fromPlainObject(deposit);
    const transaction = await this.system.makeDepositFor(
      accountId,
      moneyDeposit
    );
    return reply.status(200).send({ transactionId: transaction.id });
  }

  async withdraw(
    request: FastifyRequestWithTypeBox<WithdrawType>,
    reply: FastifyReplyWithTypeBox<WithdrawType>
  ): Promise<void> {
    const { accountId, withdraw } = request.body;
    const moneyWithdraw = Money.fromPlainObject(withdraw);
    const transaction = await this.system.makeWithdrawFor(
      accountId,
      moneyWithdraw
    );
    return reply.status(200).send({ transactionId: transaction.id });
  }

  async buy(
    request: FastifyRequestWithTypeBox<BuyType>,
    reply: FastifyReplyWithTypeBox<BuyType>
  ): Promise<void> {
    const { accountId, symbol, investment } = request.body;
    const investmentMoney = Money.fromPlainObject(investment);
    // TODO symbol + investmentMoney podrian reificarse en BuyOrder
    const transaction = await this.system.makeBuyOrder(
      accountId,
      symbol,
      investmentMoney
    );
    return reply.status(200).send(transaction);
  }
}
