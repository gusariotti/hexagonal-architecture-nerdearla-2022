import { Money, MoneyLike } from "../shared/money";
import { AccountSystemError } from "./accountErrors";

export interface AccountLike {
  id: number;
  userId: string;
  creationDate: string | Date;
  cashBalance: MoneyLike;
  valuation: MoneyLike;
}

export class Account {
  readonly id: number;
  readonly userId: string;
  readonly creationDate: Date;
  private _cashBalance: Money;
  readonly valuation: Money;

  static fromAccountLike(possibleAccount: AccountLike) {
    let creationDate: Date;
    if (typeof possibleAccount.creationDate === "string") {
      creationDate = new Date(possibleAccount.creationDate);
    } else {
      creationDate = possibleAccount.creationDate;
    }
    return new Account(
      possibleAccount.id,
      possibleAccount.userId,
      creationDate,
      Money.fromPlainObject(possibleAccount.cashBalance),
      Money.fromPlainObject(possibleAccount.valuation)
    );
  }

  constructor(
    id: number,
    userId: string,
    creationDate: Date,
    cashBalance: Money,
    valuation: Money
  ) {
    this.id = id;
    this.userId = userId;
    this.creationDate = creationDate;
    this._cashBalance = cashBalance;
    this.valuation = valuation;
  }

  get cashBalance() {
    return this._cashBalance;
  }

  makeDeposit(amount: Money) {
    this._cashBalance = this._cashBalance.add(amount);
  }

  makeWithdraw(amount: Money) {
    if (amount.greaterThan(this._cashBalance))
      throw new AccountSystemError(
        "Insufficient funds to make the withdrawal."
      );
    this._cashBalance = this._cashBalance.substract(amount);
  }
}
