import type { Ledger } from "./ledger/ledger";
import { AccountSystemError } from "./accountErrors";
import { ARS, Money } from "../shared/money";
import { v4 as uuidv4 } from "uuid";
import type { Broker } from "./broker";

// NOTE nuestro sistema, la aplicacion
export class AccountSystem {
  ledger: Ledger;
  broker: Broker;

  //NOTE instancias de Ledger y Broker son nuestros adaptadores
  constructor(accounts: Ledger, broker: Broker) {
    this.ledger = accounts;
    this.broker = broker;
  }

  async getAccount(accountId: number) {
    const account = await this.ledger.findAccountWith(accountId);
    if (account == null) throw new AccountSystemError("No account with id.");
    return account;
  }

  async createAccountFor(userId: string) {
    if (userId.trim() === "")
      throw new AccountSystemError("UserId cannot be an empty.");
    const account = await this.ledger.createAccountFor(userId);
    return account;
  }

  async makeDepositFor(accountId: number, deposit: Money) {
    // NOTE Podria haber sido ARS.isInstance(deposit)
    if (deposit.isCurrency(ARS.SYMBOL) === false)
      throw new AccountSystemError("Only ARS deposits are accepted.");

    if (deposit.isLessOrEqualThan(ARS.Zero()))
      throw new AccountSystemError(
        "Deposits must be for an amount greater than zero."
      );

    const account = await this.getAccount(accountId);
    // TODO raro, no? revisar
    account.makeDeposit(deposit);
    return this.ledger.registerCashBalance(account);
  }

  async makeWithdrawFor(accountId: number, withdraw: Money) {
    if (withdraw.isCurrency(ARS.SYMBOL) === false)
      throw new AccountSystemError("Only ARS withdrawals are accepted.");

    const account = await this.getAccount(accountId);
    // TODO raro, no? revisar
    account.makeWithdraw(withdraw);
    return this.ledger.registerCashBalance(account);
  }

  async makeBuyOrder(accountId: number, symbol: string, investment: Money) {
    if (investment.currency !== "ARS")
      throw new AccountSystemError("Only ARS investments are accepted.");

    // TODO falta algun concepto
    const { realInvestment, stockQuantity } = await this.broker.makeBuyOrder(
      symbol,
      investment
    );
    const account = await this.getAccount(accountId);

    // TODO revisar, ¿qué pasa si el monto de la inversion es mayor al balance de la cuenta?
    if (realInvestment.greaterThan(account.cashBalance)) {
      throw new AccountSystemError("Insufficient funds to make buy order.");
    } else {
      account.makeWithdraw(realInvestment);
      this.ledger.registerCashBalance(account);
      // NOTE Eso es todo? Y el registro de la inversión para la cuenta?
      // TODO deberia ser parte del Buy Order?
      return {
        transactionId: uuidv4(),
        symbol: symbol,
        buyedStock: stockQuantity,
        realInvestment: realInvestment,
      };
    }
  }
}
