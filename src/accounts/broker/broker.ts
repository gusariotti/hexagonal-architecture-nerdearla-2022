import type { Money } from "../../shared/money";

// TODO revisar conceptualmente
export class BuyOrder {
  readonly realInvestment: Money;
  readonly stockQuantity: number;

  constructor(realInvestment: Money, stockQuantity: number) {
    this.realInvestment = realInvestment;
    this.stockQuantity = stockQuantity;
  }
}

// TODO revisar conceptualmente
export class FinancialInstrumentValue {
  readonly symbol: string;
  readonly value: number;

  constructor(symbol: string, value: number) {
    this.symbol = symbol;
    this.value = value;
  }
}

export abstract class Broker {
  abstract makeBuyOrder(symbol: string, investment: Money): Promise<BuyOrder>;
  abstract getMarketValue(
    symbol: string
  ): Promise<FinancialInstrumentValue | null>;
}
