import got, { HTTPError } from "got";
import { ARS, Money } from "../../shared/money";
import { AccountSystemError } from "../accountErrors";
import { Broker, BuyOrder, FinancialInstrumentValue } from "./broker";

export class BrokerMarketStack implements Broker {
  private url: string;
  private accessToken: string;

  constructor(url: string, accessToken: string) {
    this.url = url;
    this.accessToken = accessToken;
  }
  async makeBuyOrder(symbol: string, investment: Money) {
    const symbolValue = await this.getMarketValue(symbol);
    if (symbolValue == null)
      throw new AccountSystemError("Invalid symbol provided.");

    const stockQuantity = Math.floor(investment.amount / symbolValue.value);
    if (stockQuantity < 1) {
      throw new AccountSystemError("Insufficient investment to buy an stock.");
    }

    const realInvestment = new ARS(stockQuantity * symbolValue.value);
    // TODO reificar
    return new BuyOrder(realInvestment, stockQuantity);
  }

  async getMarketValue(
    symbol: string
  ): Promise<FinancialInstrumentValue | null> {
    try {
      const response: any = await got(this.url, {
        searchParams: {
          access_key: this.accessToken,
          symbols: symbol,
          sort: "DESC",
        },
      }).json();

      let symbolInfo: any;
      let symbolValue = 0;
      if (
        response != null &&
        typeof response === "object" &&
        "data" in response &&
        Array.isArray(response.data)
      ) {
        symbolInfo = response.data[0];
        symbolValue = (symbolInfo.high + symbolInfo.low) / 2;
      }
      return new FinancialInstrumentValue(symbol, symbolValue);
    } catch (reqError) {
      if (reqError instanceof Error && reqError.name === "HTTPError") {
        const { error } = (reqError as HTTPError).response.body as any;
        if (
          error != null &&
          typeof error === "object" &&
          "code" in error &&
          error.code === "no_valid_symbols_provided"
        ) {
          return null;
        }
      }
      throw reqError;
    }
  }
}
