// NOTE Error propio del sistema
export class AccountSystemError extends Error {
  constructor(message: string) {
    super(message);
    // because we are extending a built-in class
    Object.setPrototypeOf(this, AccountSystemError.prototype);
  }
}
