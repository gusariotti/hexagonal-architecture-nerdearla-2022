import type { FastifyServerOptions } from "fastify";
import type { Connection } from "mongoose";
import { AccountSystem } from "../accounts/accountSystem";
import { BrokerMarketStack } from "../accounts/broker";
import { MongooseLedger } from "../accounts/ledger";
import { Clock } from "./clock";
import { connect, connection } from "mongoose";

export interface MongooseOptions {
  connection: Connection;
  forceClose?: boolean;
}

export class AppOptions {
  readonly fastifyServer: FastifyServerOptions | undefined;
  readonly mongoose: MongooseOptions | undefined;
  readonly clock: Clock;
  readonly accountSystem: AccountSystem;

  constructor(
    clock: Clock,
    accountSystem: AccountSystem,
    fastifyOption: FastifyServerOptions | undefined,
    mongoose: MongooseOptions | undefined
  ) {
    this.clock = clock;
    this.accountSystem = accountSystem;
    this.fastifyServer = fastifyOption;
    this.mongoose = mongoose;
  }
}

export class AppOptionsBuilder {
  static defaultOptions(): AppOptions {
    const fastifyOptions = {};
    connect("mongodb://127.0.0.1:27017/accounts");
    const mongooseOptions: MongooseOptions = {
      connection: connection,
      forceClose: true,
    };
    const clock = new Clock();
    const accounts = new MongooseLedger(clock);
    const url = "http://api.marketstack.com/v1/eod/latest";
    const token = process.env["MS_ACCESS_TOKEN"] ?? "NOT_A_TOKEN";
    const broker = new BrokerMarketStack(url, token);
    const system = new AccountSystem(accounts, broker);
    return new AppOptions(clock, system, fastifyOptions, mongooseOptions);
  }
}
