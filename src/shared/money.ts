export interface MoneyLike {
  currency: string;
  amount: number;
}
export class Money {
  // NOTE para transformar objetos planos a instancias
  static isMoneyLike(
    possibleMoneyObject: unknown
  ): possibleMoneyObject is MoneyLike {
    let hasCurrencyString = false;
    let hasAmountNumber = false;

    if (
      typeof possibleMoneyObject === "object" &&
      possibleMoneyObject !== null &&
      !Array.isArray(possibleMoneyObject) &&
      "currency" in possibleMoneyObject &&
      "amount" in possibleMoneyObject
    ) {
      hasCurrencyString =
        typeof (possibleMoneyObject as MoneyLike).currency === "string";
      hasAmountNumber =
        typeof (possibleMoneyObject as MoneyLike).currency === "string";
    }
    return hasCurrencyString && hasAmountNumber;
  }

  static fromPlainObject(possibleMoneyObject: unknown): Money {
    if (Money.isMoneyLike(possibleMoneyObject)) {
      return new Money(
        possibleMoneyObject.amount,
        possibleMoneyObject.currency
      );
    }
    throw TypeError(`Error! Can't instanciate as Money. Not MoneyLike!`);
  }
  readonly currency: string;
  readonly amount: number;
  static SYMBOL: string;

  static Zero() {
    // WARNING
    return new this(0, this.SYMBOL);
  }
  constructor(amount: number, currency: string) {
    this.currency = currency;
    this.amount = amount;
  }

  isCurrency(symbol: string) {
    return this.currency === symbol;
  }

  equals(otherMoney: Money) {
    return (
      this.currency === otherMoney.currency && this.amount === otherMoney.amount
    );
  }

  add(someMoney: Money) {
    if (this.isCurrency(someMoney.currency)) {
      return new Money(this.amount + someMoney.amount, this.currency);
    }
    throw new TypeError("ERROR! Is not possible to add different currencies");
  }

  substract(someMoney: Money): Money {
    if (this.isCurrency(someMoney.currency)) {
      return new Money(this.amount - someMoney.amount, this.currency);
    }
    throw new TypeError("ERROR! Is not possible to add different currencies");
  }

  greaterThan(someMoney: Money) {
    if (this.isCurrency(someMoney.currency)) {
      return this.amount > someMoney.amount;
    }
    throw new TypeError(
      "ERROR! Is not possible to compare amounts of different currencies."
    );
  }

  isLessOrEqualThan(someMoney: Money) {
    if (this.isCurrency(someMoney.currency)) {
      return this.amount <= someMoney.amount;
    }
    throw new TypeError(
      "ERROR! Is not possible to compare amounts of different currencies."
    );
  }
}

export class ARS extends Money {
  static override SYMBOL = "ARS";

  constructor(amount: number) {
    super(amount, ARS.SYMBOL);
  }
}

export class USD extends Money {
  static override SYMBOL = "USD";
  constructor(amount: number) {
    super(amount, USD.SYMBOL);
  }
}
