"use strict";
import type { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";
import type {
  FastifyBaseLogger,
  FastifyInstance,
  FastifyReply,
  FastifyRequest,
  FastifySchema,
  FastifyTypeProvider,
  RawReplyDefaultExpression,
  RawRequestDefaultExpression,
  RawServerDefault,
  RouteGenericInterface,
  RouteHandlerMethod,
  RouteOptions,
} from "fastify";

// Types extendidos con TypeProvider para
// evitar escribir todos los parametros genericos

// Fastify instance
export type FastifyInstanceWithTypeProvider<
  TypeProvider extends FastifyTypeProvider
> = FastifyInstance<
  RawServerDefault,
  RawRequestDefaultExpression,
  RawReplyDefaultExpression,
  FastifyBaseLogger,
  TypeProvider
>;
export type FastifyInstanceWithTypeBox =
  FastifyInstanceWithTypeProvider<TypeBoxTypeProvider>;

// RouteOptions
export type RouteOptionsWithTypeProvider<
  RouteInterface extends RouteGenericInterface,
  TypeProvider extends FastifyTypeProvider
> = RouteOptions<
  RawServerDefault,
  RawRequestDefaultExpression,
  RawReplyDefaultExpression,
  RouteInterface,
  unknown,
  FastifySchema,
  TypeProvider
>;
export type RouteOptionsWithTypeBox<
  RouteInterface extends RouteGenericInterface
> = RouteOptionsWithTypeProvider<RouteInterface, TypeBoxTypeProvider>;

// Route Handler
export type RouteHandlerMethodWithTypeProvider<
  RouteInterface extends RouteGenericInterface,
  TypeProvider extends FastifyTypeProvider
> = RouteHandlerMethod<
  RawServerDefault,
  RawRequestDefaultExpression,
  RawReplyDefaultExpression,
  RouteInterface,
  unknown,
  FastifySchema,
  TypeProvider
>;
export type RouteHandlerMethodWithTypeBox<
  RouteInterface extends RouteGenericInterface
> = RouteHandlerMethodWithTypeProvider<RouteInterface, TypeBoxTypeProvider>;

// Request
export type FastifyRequestWithTypeProvider<
  RouteInterface extends RouteGenericInterface,
  TypeProvider extends FastifyTypeProvider
> = FastifyRequest<
  RouteInterface,
  RawServerDefault,
  RawRequestDefaultExpression,
  FastifySchema,
  TypeProvider,
  unknown,
  FastifyBaseLogger
>;
export type FastifyRequestWithTypeBox<
  RouteInterface extends RouteGenericInterface
> = FastifyRequestWithTypeProvider<RouteInterface, TypeBoxTypeProvider>;

// Reply
export type FastifyReplyWithTypeProvider<
  RouteInterface extends RouteGenericInterface,
  TypeProvider extends FastifyTypeProvider
> = FastifyReply<
  RawServerDefault,
  RawRequestDefaultExpression,
  RawReplyDefaultExpression,
  RouteInterface,
  unknown,
  FastifySchema,
  TypeProvider
>;
export type FastifyReplyWithTypeBox<
  RouteInterface extends RouteGenericInterface
> = FastifyReplyWithTypeProvider<RouteInterface, TypeBoxTypeProvider>;
