import { buildUsing } from "./app";
import { AppOptions, AppOptionsBuilder } from "./shared/appOptions";

// TODO crear clase para generar configuracion
const options: AppOptions = AppOptionsBuilder.defaultOptions();

// NOTE podria ser una clase (WebServer) que aisle de las implementaciones
//      concretas como Fastify (ej: FastifyWebServer)
const server = buildUsing(options);
const start = async () => {
  try {
    await server.listen({ port: 3000 });

    const address = server.server.address();
    const _port = typeof address === "string" ? address : address?.port;
    console.log(`It's on! Listening on port ${_port}`);
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};
void start();
