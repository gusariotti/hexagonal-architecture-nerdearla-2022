"use strict";
import Fastify from "fastify";
import type { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";
import { AccountRoutes } from "./accounts/accountsRoutes";
import type { FastifyInstanceWithTypeBox } from "./shared/types";
import type { AppOptions } from "./shared/appOptions";
import { AccountSystemError } from "./accounts/accountErrors";

export function buildUsing(options: AppOptions): FastifyInstanceWithTypeBox {
  // NOTE Tipo custom para Fastify con TypeBox (ver shared/types)
  const app: FastifyInstanceWithTypeBox = Fastify(
    options?.fastifyServer
  ).withTypeProvider<TypeBoxTypeProvider>();

  // NOTE Cierra las conexiones al momento de cerrar la instancia de Fastify
  if (options.mongoose != null) {
    const { connection, forceClose } = options.mongoose;
    app.addHook("onClose", () => connection.close(forceClose));
  }
  const accountRoutes = AccountRoutes.using(options);
  app.register(accountRoutes.routes());
  // NOTE Handler para los nuevos errores
  app.setErrorHandler(function (error, _request, reply) {
    if (error instanceof AccountSystemError) {
      reply.status(400).send({
        statusCode: 400,
        error: "Bad Request",
        message: error.message,
      });
    } else {
      throw error;
    }
  });

  return app;
}
