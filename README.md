# hexagonal-architecture-nerdearla-2022

Proyecto presentado en el workshop ["Arquitectura Hexagonal: Una perspectiva en NodeJS"](https://www.youtube.com/live/-9Q93Zl12eM?feature=share) en Nerdearla 2022.

## Stack

El proyecto utiliza principalmente lo siguiente:

- [Fastify](https://www.fastify.io/)
- [TypeScript](https://www.typescriptlang.org/)
- [Jest](https://jestjs.io/) + [jest-extended](https://www.npmjs.com/package/jest-extended) + [@shelf/jest-mongodb](https://www.npmjs.com/package/@shelf/jest-mongodb)
- [Moongose](https://mongoosejs.com/)
- [got](https://github.com/sindresorhus/got/)
- [@sinclair/typebox](https://www.npmjs.com/package/@sinclair/typebox)
- [husky](https://github.com/typicode/husky)

## Test de código

Uno de los motivos por el cual no publicamos antes el repositorio (mil disculpas) es que queríamos publicarlo con tests de código de la aplicación, el `AccountSystem`.

Esos tests están en [test/accountsSystem.dev.test.ts](./test/accountsSystem.dev.test.ts).

Si los comparas con los tests de integración, que fue los que utilizamos en el workshop, verás que estos son mucho más sencillos y breves en comparación. Incluso permitieron encontrar algunos casos borde que con los tests de integración no se detectaron.

### IMPORTANTE

Los tests no validan que todo lo que deberían, en particular la asociación entre una orden de compra y la cuenta, y eso se nota en el código resultante. Algunos motivos puedes encontrarlos en el [FAQ](#faq).

Esto no hace que testear no sea importante, todo lo contrario, muestra que debe tenerse en cuenta que se está probando y que hay que verificar en la prueba.

Dejamos algunas notas y comentarios al respecto que esperamos ayuden. También puede ser un lindo ejercicio sobre el código. 

## TAGS

El siguiente es el listado de tags utilizados durante el workshop para ir avanzando con los refactors. Puedes moverte entre ellos mediante tu IDE favorita o mediante el comando:
`git checkout tags/{nombre-del-tag}` reemplazando `{nombre-del-tag} por el tag en cuestión.

También es posible navegar por el código desde GitLab.com.

- [01-inicio](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/01-inicio)
- [02-extraer-error-sistema](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/02-extraer-error-sistema)
- [03-refactor-final-replies-por-errores-sistema](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/03-refactor-final-replies-por-errores-sistema)
- [04-extraccion-accion-obtener-cuenta](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/04-extraccion-accion-obtener-cuenta)
- [04b-mover-account-system](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/04b-mover-account-system)
- [05-extraccion-accion-obtener-cuenta](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/05-extraccion-accion-obtener-cuenta)
- [06-extraccion-accion-deposito](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/06-extraccion-accion-deposito)
- [07-tratar-de-no-romper-encapsulamiento](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/07-tratar-de-no-romper-encapsulamiento)
- [08-todas-las-acciones-extraidas](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/08-todas-las-acciones-extraidas)
- [09-account-system-como-property-controlador](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/09-account-system-como-property-controlador)
- [10-encapsulamiento-mongoose-clase](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/10-encapsulamiento-mongoose-clase)
- [11-mover-creacion-cuenta-a-clase-mongoose](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/11-mover-creacion-cuenta-a-clase-mongoose)
- [12-metodos-mongoose-encapsulados](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/12-metodos-mongoose-encapsulados)
- [13-mover-creacion-mongoose-account-controller](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/13-mover-creacion-mongoose-account-controller)
- [14-creacion-broker](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/14-creacion-broker)
- [15-mover-broker-a-submodulo-y-abstraer](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/15-mover-broker-a-submodulo-y-abstraer)
- [16-mover-mongoose-a-modulo-ledger](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/16-mover-mongoose-a-modulo-ledger)
- [extra-01-encapsulamiento-opciones](https://gitlab.com/gusariotti/hexagonal-architecture-nerdearla-2022/-/tags/extra-01-encapsulamiento-opciones)

## FAQ

- ¿Cuál era el objetivo del Workshop?

    Presentar que es arquitectura hexagonal y mostrar como es posible llevar un proyecto existente de Web API con Fastify, el cuál por suerte tiene tests, a uno con Arquitectura Hexagonal.

- ¿Por qué hicieron X? ¿Por qué usaron Y así?

    El proyecto no intenta ser perfecto, si no por el contrario intenta de cierta manera emular un proyecto como podría aparecer en la práctica: incompleto, con errores, con cosas para mejorar. 
    
    Ese es un poco el objetivo de llevarlo hacia una arquitectura hexagonal, ayudar a mejorarlo, facilitar los cambios. 
    
    También es cierto que no nos dió el tiempo para hacer todo lo que queríamos hacer y mostrar :_( y por supuesto somos humanos y nos podemos equivocar.

- ¿Por qué hay comentarios `// NOTE`?

    Son comentarios con alguna observación. Servían como recordatorios durante el workshop. Los dejamos porque nos parecen que pueden aportar a entender el código.

- ¿Por qué hay comentarios `// TODO`?

    Son comentarios de cosas que están incompletas dentro del código y que podrían hacerse o verse.


Copyright Naranja X – Todos los derechos reservados