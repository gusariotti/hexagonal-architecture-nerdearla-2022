import type { JestConfigWithTsJest } from "ts-jest/dist/types";
import { defaults as tsjPreset } from "ts-jest/presets";

// import { defaultsESM as tsjPreset } from 'ts-jest/presets'
// import { jsWithTs as tsjPreset } from 'ts-jest/presets'
// import { jsWithTsESM as tsjPreset } from 'ts-jest/presets'
// import { jsWithBabel as tsjPreset } from 'ts-jest/presets'
// import { jsWithBabelESM as tsjPreset } from 'ts-jest/presets'

const config: JestConfigWithTsJest = {
  ...tsjPreset,
  transform: {
    ...tsjPreset.transform,
    // [...]
  },
  setupFilesAfterEnv: ["jest-extended/all"],
  testRegex: "/test/.*.dev.test.[jt]sx?$",
};

export default config;
